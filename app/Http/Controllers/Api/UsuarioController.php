<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Usuario;

class UsuarioController extends Controller
{
    public function listar()
    {
        $usuarios = Usuario::all()->toJson(JSON_PRETTY_PRINT);

        return response($usuarios, 200, ['Content-Type' => 'application/json; charset=utf-8']);
    }


    public function cadastrar(Request $request)
    {
        $usuario = Usuario::create($request->all());

        if ($usuario) {
            $usuario = $usuario->toJson(JSON_PRETTY_PRINT);
            return response($usuario, 200, ['Content-Type' => 'application/json; charset=utf-8']);
        } else {
            return response()->json([
                "message" => "Falha ao cadastrar usuário!"
            ], 403);
        }
    }


    public function mostrar($id)
    {
        $usuario = Usuario::find($id);

        if ($usuario) {
            return response()->json($usuario, 200, ['Content-Type' => 'application/json; charset=utf-8'], JSON_PRETTY_PRINT);
        } else {
            return response()->json([
                "message" => "Usuário não encontrado!"
            ], 404);
        }
    }


    public function editar(Request $request, $id)
    {
        $usuario = Usuario::find($id);

        if ($usuario) {
            $updated = $usuario->update($request->all());

            if ($updated) {
                return response()->json($usuario, 200, ['Content-Type' => 'application/json; charset=utf-8'], JSON_PRETTY_PRINT);
            } else {
                return response()->json([
                    "message" => "Falha ao atualizar usuário!"
                ], 403, ['Content-Type' => 'application/json; charset=utf-8']);
            }

            $usuario = $usuario->toJson(JSON_PRETTY_PRINT);
            return response($usuario, 200);
        } else {
            return response()->json([
                "message" => "Usuário não encontrado!"
            ], 404, ['Content-Type' => 'application/json; charset=utf-8']);
        }
    }


    public function deletar($id)
    {
        $usuario = Usuario::find($id);

        if ($usuario) {
            $deleted = $usuario->delete();

            if ($deleted) {
                return response()->json([
                    "message" => "Usuário excluído com sucesso!"
                ], 200, ['Content-Type' => 'application/json; charset=utf-8']);
            } else {
                return response()->json([
                    "message" => "Falha ao deletar usuário!"
                ], 403, ['Content-Type' => 'application/json; charset=utf-8']);
            }
        } else {
            return response()->json([
                "message" => "Usuário não encontrado!"
            ], 404, ['Content-Type' => 'application/json; charset=utf-8']);
        }
    }
}
