<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\UsuarioController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('usuarios/listar', [UsuarioController::class, 'listar']);
Route::post('usuarios/cadastrar', [UsuarioController::class, 'cadastrar']);
Route::get('usuarios/mostrar/{id}', [UsuarioController::class, 'mostrar']);
Route::put('usuarios/editar/{id}', [UsuarioController::class, 'editar']);
Route::delete('usuarios/deletar/{id}', [UsuarioController::class, 'deletar']);
